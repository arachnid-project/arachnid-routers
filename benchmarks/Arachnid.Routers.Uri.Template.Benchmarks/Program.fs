﻿// Arachnid

open Arachnid.Core
open Arachnid.Routers.Uri.Template

let value i =
    arachnid {
        do! Arachnid.Optic.set (State.value_ "test") (Some i) }

let router =
    arachnidRouter {
        resource "/" (value 0)
        resource "/one" (value 1)
        resource "/one/{two}" (value 2) }

let system =
    Arachnid.infer router

// Benchmark

open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Running
open Arachnid.Optics.Http
open Arachnid.Testing

let path p =
    arachnid {
        do! Arachnid.Optic.set Request.path_ p }

let paths =
    List.map path [
        "/"
        "/one"
        "/one/two" ]

type Benchmarks () =

    [<Benchmark>]
    member __.Basic () =
        List.map (flip evaluate system) paths

// Configuration

open BenchmarkDotNet.Configs
open BenchmarkDotNet.Diagnosers
open BenchmarkDotNet.Exporters
open BenchmarkDotNet.Jobs
open BenchmarkDotNet.Validators

let configuration =

    ManualConfig
        .Create(DefaultConfig.Instance)
        .With(Job.MediumRun.WithGcServer(true))
        .With(MemoryDiagnoser.Default)
        .With(MemoryDiagnoser.Default)
        .With(BaselineValidator.FailOnError)
        .With(JitOptimizationsValidator.FailOnError)
        .With(MarkdownExporter.GitHub)

// Main

[<EntryPoint>]
let main _ =

    let _ = BenchmarkRunner.Run<Benchmarks> (configuration)

    0
