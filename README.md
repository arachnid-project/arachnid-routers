# Freya Routers

## Overview

Freya Routers provides powerful routing tools and abstractions for web routing, integrating seamlessly with the Freya pipeline model.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-routers/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-routers/commits/master)

## See Also

For more information see the [meta-repository for the Freya Web Stack](https://github.com/xyncro/freya), along with the main [freya.io](https://freya.io) site.
