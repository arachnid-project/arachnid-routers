﻿[<AutoOpen>]
module Arachnid.Routers.Uri.Template.Tests.Prelude

open Aether
open Arachnid.Core

// Optics

let defaultValue : Lens<State,string option> =
    State.value_ "default"

// Fixtures

let defaultSetup =
    Arachnid.empty
