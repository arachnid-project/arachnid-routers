﻿[<AutoOpen>]
module Arachnid.Routers.Uri.Template.Tests.Matching

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Optics.Http
open Arachnid.Routers.Uri.Template
open Arachnid.Testing
open Arachnid.Testing.Operators
open Arachnid.Types.Http
open Xunit
//* Matching

// Tests covering basic matching of routes to paths and methods. Capture of
// matched variables is tested explicitly elsewhere.

[<Fact>]
let ``Router With Root Route Executes Correctly`` () =

    let router =
        arachnidRouter {
            route All "/" (defaultValue .= Some "test") }

    verify defaultSetup router [
        defaultValue => Some "test" ]

[<Fact>]
let ``Router With Multiple Routes Executes Correctly`` () =

    let router =
        arachnidRouter {
            route All "/" (defaultValue .= Some "one")
            route All "/some/path" (defaultValue .= Some "two")
            route All "/other/path" (defaultValue .= Some "three") }

    verify (Request.path_ .= "/") router [
        defaultValue => Some "one" ]

    verify (Request.path_ .= "/some/path") router [
        defaultValue => Some "two" ]

    verify (Request.path_ .= "/other/path") router [
        defaultValue => Some "three" ]

[<Fact>]
let ``Router With Method Specific Routes Executes Correctly`` () =

    let router =
        arachnidRouter {
            route GET "/" (defaultValue .= Some "one")
            route GET "/some/path" (defaultValue .= Some "two")
            route POST "/some/path" (defaultValue .= Some "three") }

    verify (Request.method_ .= GET) router [
        defaultValue => Some "one" ]

    verify (Request.method_ .= POST) router [
        defaultValue => None ]

    verify ((Request.method_ .= GET) *> (Request.path_ .= "/some/path")) router [
        defaultValue => Some "two" ]

    verify ((Request.method_ .= POST) *> (Request.path_ .= "/some/path")) router [
        defaultValue => Some "three" ]

[<Fact>]
let ``Router Executes Pipeline Registered First`` () =

    let router =
        arachnidRouter {
            route GET "/" (defaultValue .= Some "one")
            route All "/" (defaultValue .= Some "two") }

    verify defaultSetup router [
        defaultValue => Some "one" ]

[<Fact>]
let ``Router Executes First Full Match`` () =

    let router =
        arachnidRouter {
            route All "/{one}/a" (defaultValue .= Some "one")
            route All "/{two}/b" (defaultValue .= Some "two")
            route All "/{one}/b" (defaultValue .= Some "three") }

    // TODO: Move the tests for route capture to a distinct file and set of tests!

    verify (Request.path_ .= "/some/b") router [
        Route.atom_ "one" => None
        Route.atom_ "two" => Some "some"
        defaultValue => Some "two" ]

[<Fact>]
let ``Router Matches Optional Path Segment`` () =

    let router =
        arachnidRouter {
            route All "/test{/test}" (defaultValue .= Some "one") }

    verify (Request.path_ .= "/test") router [
        Route.atom_ "test" => None
        defaultValue => Some "one" ]

    verify (Request.path_ .= "/test/test") router [
        Route.atom_ "test" => Some "test"
        defaultValue => Some "one" ]
